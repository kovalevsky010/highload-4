require('dotenv').config();

const createKafkaProducerConfig = () => {
  const bootstrapServers = process.env.BOOTSTRAP_SERVERS;

  if (!bootstrapServers) {
    throw new Error('BOOTSTRAP_SERVERS variable is required');
  }

  return {
    'bootstrap.servers': bootstrapServers,
    'dr_msg_cb': true,
  }
}

const createKafkaConsumerConfig = () => {
  const bootstrapServers = process.env.BOOTSTRAP_SERVERS;

  if (!bootstrapServers) {
    throw new Error('BOOTSTRAP_SERVERS variable is required');
  }

  return {
    'bootstrap.servers': process.env.BOOTSTRAP_SERVERS,
    'group.id': 'kafka-nodejs-getting-started',
  }
};

module.exports = {
  createKafkaProducerConfig,
  createKafkaConsumerConfig,
  TOPIC_NAME: 'test_topic'
}