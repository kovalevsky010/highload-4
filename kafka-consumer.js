const Kafka = require('node-rdkafka');
const { createKafkaConsumerConfig, TOPIC_NAME } = require('./create-kafka-config');


const createKafkaConsumer = (config, onData) => {
  const consumer = new Kafka.KafkaConsumer(config, {'auto.offset.reset': 'earliest'});

  return new Promise((resolve, reject) => {
    consumer
      .on('ready', () => resolve(consumer))
      .on('data', onData);

    consumer.connect();
  });
}


const consume = async () => {
  const topic = TOPIC_NAME;
  const config = createKafkaConsumerConfig();

  const consumer = await createKafkaConsumer(config, ({ value }) => {
    console.log(`Consumed event from topic ${topic}: message: ${value}`);
  });

  consumer.subscribe([topic]);
  consumer.consume();

  process.on('SIGINT', () => {
    console.log('\nDisconnecting consumer ...');
    consumer.disconnect();
  });
}

consume()
  .catch((err) => {
    console.error(`Something went wrong:\n${err}`);
    process.exit(1);
  });
