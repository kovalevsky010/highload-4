const Kafka = require('node-rdkafka');
const { createKafkaProducerConfig, TOPIC_NAME } = require('./create-kafka-config');

const createKafkaProducer = (config, onDeliveryReport) => {
  const producer = new Kafka.Producer(config);

  return new Promise((resolve, reject) => {
    producer
      .on('ready', () => resolve(producer))
      .on('delivery-report', onDeliveryReport)
      .on('event.error', (err) => {
        console.error('event.error', err);
        reject(err);
      });

    producer.connect();
  });
}

const produce = async () => {
  const config = createKafkaProducerConfig();
  const topic = TOPIC_NAME;
  const countMessages = 1000;

  const producer = await createKafkaProducer(config, (err, report) => {
    if (err) {
      console.warn('Error producing', err)
    } else {
      const { topic, value } = report;
      console.log(`Produced event to topic ${topic} at: ${value}`);
    }
  });

  for (let i = 0; i < countMessages; ++i) {
    const message = Buffer.from(new Date().toUTCString());
    producer.produce(topic, -1, message);
  }

  producer.flush(10000, () => {
    producer.disconnect();
  });
}

produce()
  .catch((err) => {
    console.error(`Something went wrong:\n${err}`);
    process.exit(1);
  });
